package com.brevitaz.multithreading;

import com.brevitaz.demo.streamapi.models.Customer;
import com.brevitaz.demo.streamapi.models.Order;
import com.brevitaz.demo.streamapi.models.Product;
import com.brevitaz.demo.streamapi.repos.CustomerRepo;
import com.brevitaz.demo.streamapi.repos.OrderRepo;
import com.brevitaz.demo.streamapi.repos.ProductRepo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

import static java.util.stream.Collectors.*;


@Slf4j
@RunWith(SpringRunner.class)
@DataJpaTest
public class StreamAPISolution {

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private ProductRepo productRepo;

    @Test
    @DisplayName("Obtain a list of product with category = \"Books\" and price > 100")
    public void exercise1() {
        long startTime = System.currentTimeMillis();
        List<Product> result = productRepo.findAll()
                .stream()
                .filter(p -> p.getCategory().equalsIgnoreCase("Books"))
                .filter(p -> p.getPrice() > 100)
                .toList();
        long endTime = System.currentTimeMillis();

        log.info(String.format("exercise 1 - execution time: %1$d ms", (endTime - startTime)));
        result.forEach(p -> log.info(p.toString()));

    }

    @Test
    @DisplayName("Obtain a list of product with category = \"Books\" and price > 100 (using Predicate chaining for filter)")
    public void exercise1a() {
        List<Product> result = productRepo.findAll()
                .stream()
                .filter(product -> product.getCategory().equalsIgnoreCase("Books") && product.getPrice() > 100)
                .toList();
        result.forEach(p -> log.info(p.toString()));
    }

    @Test
    @DisplayName("Obtain a list of product with category = \"Books\" and price > 100 (using BiPredicate for filter)")
    public void exercise1b() {
        BiPredicate<String,Double> biPredicate = (category,price)->category.equalsIgnoreCase("Books") && price > 100;
        List<Product> result = productRepo.findAll()
                .stream()
                .filter(product -> biPredicate.test(product.getCategory(), product.getPrice()))
                .toList();
        result.forEach(p -> log.info(p.toString()));
    }

    @Test
    @DisplayName("Obtain a list of order with product category = \"Baby\"")
    public void exercise2() {
        List<Order> result = orderRepo.findAll()
                .stream()
                .filter(order -> order.getProducts().stream().anyMatch(product -> product.getCategory().equalsIgnoreCase("Baby")))
                .toList();
        result.forEach(p -> log.info(p.toString()));
    }

    @Test
    @DisplayName("Obtain a list of product with category = “Toys” and then apply 10% discount\"")
    public void exercise3() {
        List<Product> result = productRepo.findAll()
                .stream()
                .filter(product -> product.getCategory().equalsIgnoreCase("Toys"))
                .map(product -> {
                    product.setPrice(product.getPrice() * 0.9);
                    return product;
                }).toList();
        result.forEach(p -> log.info(p.toString()));
    }

    @Test
    @DisplayName("Obtain a list of products ordered by customer of tier 2 between 01-Feb-2021 and 01-Apr-2021")
    public void exercise4() {
        List<Product> result = orderRepo.findAll()
                .stream()
                .filter(order -> order.getCustomer().getTier() == 2)
                .filter(order -> order.getOrderDate().isAfter(LocalDate.of(2021, Month.FEBRUARY, 01)))
                .filter(order -> order.getOrderDate().isBefore(LocalDate.of(2021, Month.APRIL, 01)))
                .flatMap(order -> order.getProducts().stream().distinct())
                .toList();
        result.forEach(p -> log.info(p.toString()));
    }

    @Test
    @DisplayName("Get the 3 cheapest products of \"Books\" category")
    public void exercise5() {
        List<Product> result = productRepo.findAll()
                .stream()
                .filter(product -> product.getCategory().equalsIgnoreCase("Books"))
                .sorted(Comparator.comparing(Product::getPrice))
                .limit(3)
                .toList();
        result.forEach(p -> log.info(p.toString()));
    }

    @Test
    @DisplayName("Get the 3 most recent placed order")
    public void exercise6() {
        List<Order> result = orderRepo.findAll()
                .stream()
                .sorted(Comparator.comparing(Order::getOrderDate).reversed())
                .limit(3)
                .toList();
        result.forEach(p -> log.info(p.toString()));
    }

    @Test
    @DisplayName("Get a list of products which was ordered on 15-Mar-2021")
    public void exercise7() {
        List<Product> result = orderRepo.findAll()
                .stream()
                .filter(order -> order.getOrderDate().isEqual(LocalDate.of(2021, Month.MARCH, 15)))
                .flatMap(o -> o.getProducts().stream())
                .distinct()
                .toList();
        result.forEach(p -> log.info(p.toString()));
    }

    @Test
    @DisplayName("Calculate the total lump of all orders placed in Feb 2021")
    public void exercise8() {
        long result = orderRepo.findAll()
                .stream()
                .filter(order -> order.getOrderDate().getMonthValue() == 2 && order.getOrderDate().getYear()==2021)
                .count();
        log.info(String.valueOf(result));
    }

    @Test
    @DisplayName("Calculate the average price of all orders placed on 15-Mar-2021")
    public void exercise9() {
        Double result = orderRepo.findAll()
                .stream()
                .filter(order -> order.getOrderDate().isEqual(LocalDate.of(2021, Month.MARCH, 15)))
                .map(order -> order.getProducts().stream().flatMapToDouble(product -> DoubleStream.of(product.getPrice())).sum())
                .mapToDouble(Number::doubleValue)
                .average()
                .getAsDouble();
        log.info(String.valueOf(result));
    }

    @Test
    @DisplayName("Obtain statistics summary of all products belong to \"Books\" category")
    public void exercise10() {
        DoubleSummaryStatistics summaryStatistics = productRepo.findAll()
                .stream()
                .filter(p -> p.getCategory().equalsIgnoreCase("Books"))
                .mapToDouble(Product::getPrice)
                .summaryStatistics();
        log.info(summaryStatistics.toString());
    }

    @Test
    @DisplayName("Obtain a mapping of order id and the order's product count")
    public void exercise11() {
        Map<Long,Long> result = orderRepo.findAll()
                .stream()
                .collect(Collectors.toMap(Order::getId, order -> (long) order.getProducts().size()));
        log.info(String.valueOf(result));
    }

    @Test
    @DisplayName("Obtain a data map of customer and list of orders")
    public void exercise12() {
        Map<Customer,List<Order>> result = orderRepo.findAll()
                .stream()
                .collect(groupingBy(Order::getCustomer));
        log.info(String.valueOf(result));
    }

    @Test
    @DisplayName("Obtain a data map of customer_id and list of order_id(s)")
    public void exercise12a() {
        Map<Long,List<Long>> result = orderRepo.findAll()
                .stream()
                .collect(groupingBy(Order::getCustomer))
                .entrySet()
                .stream()
                .collect(toMap(customerListEntry -> customerListEntry.getKey().getId(),customerListEntry -> customerListEntry.getValue()
                        .stream()
                        .map(Order::getId)
                        .collect(toList())));
        log.info(String.valueOf(result));
    }

    @Test
    @DisplayName("Obtain a data map with order and its total price")
    public void exercise13() {
        Map<Order,Double> result = orderRepo.findAll()
                .stream()
                .collect(Collectors.toMap(order->order, order -> order.getProducts().stream().flatMapToDouble(product -> DoubleStream.of(product.getPrice())).sum()));
        log.info(String.valueOf(result));
    }

    @Test
    @DisplayName("Obtain a data map with order and its total price (using reduce)")
    public void exercise13a() {
        Map<Order,Double> result = orderRepo.findAll()
                .stream()
                .collect(Collectors.toMap(order -> order, order -> order.getProducts()
                        .stream()
                        .map(Product::getPrice)
                        .reduce(0.0, Double::sum)));
        log.info(String.valueOf(result));
    }

    @Test
    @DisplayName("Obtain a data map of product name by category")
    public void exercise14() {
        Map<String,String> result = productRepo.findAll()
                .stream()
                .collect(Collectors.toMap(Product::getName, Product::getCategory));
        log.info(String.valueOf(result));
    }

    @Test
    @DisplayName("Get the most expensive product per category")
    void exercise15() {
        Map<String,Product> result = productRepo.findAll()
                .stream()
                .collect(Collectors.toMap(Product::getCategory, Function.identity(), BinaryOperator.maxBy(Comparator.comparing(Product::getPrice))));
        log.info(String.valueOf(result));
    }

    @Test
    @DisplayName("Get the most expensive product (by name) per category")
    void exercise15a() {
        Map<String,String> result = productRepo.findAll()
                .stream()
                .collect(Collectors.toMap(Product::getCategory, product -> product,BinaryOperator.maxBy(Comparator.comparing(Product::getPrice))))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, stringProductEntry -> stringProductEntry.getValue().getName()));
        log.info(String.valueOf(result));
    }
}
