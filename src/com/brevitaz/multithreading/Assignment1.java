//ASSIGNMENT-1: Count the number of words from 10 text files
package com.brevitaz.multithreading;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;


public class Assignment1 {
    public static void main(String[] args) {
        String myDirectoryPath = "src/com/brevitaz/multithreading/Resources";
        AtomicLong wordCounter = new AtomicLong(0);
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        File dir = new File(myDirectoryPath);
        File[] directoryListing = dir.listFiles();

        if (directoryListing != null) {
            for (File file : directoryListing) {
                String path = file.getAbsolutePath();
                executorService.submit(new WordCountTask(path, wordCounter));
            }
        } else {
            System.out.println("Directory is empty.");
        }

        executorService.shutdown();
        try {
            executorService.awaitTermination(100, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();

        }
        System.out.println(wordCounter);
    }

    static class WordCountTask implements Runnable {
        final String filePath;
        final AtomicLong counter;

        public WordCountTask(String filePath, AtomicLong counter) {
            this.filePath = filePath;
            this.counter = counter;
        }

        public long countFileWords(String path) {
            File file = new File(path);
            int wordCount = 0;
            try (Scanner sc = new Scanner(new FileInputStream(file))) {
                while (sc.hasNext()) {
                    sc.next();
                    wordCount++;
                }
                return wordCount;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return wordCount;
        }

        @Override
        public void run() {
            // count the words in filePath
            Long count = countFileWords(filePath);
            counter.addAndGet(count);
        }
    }

}
