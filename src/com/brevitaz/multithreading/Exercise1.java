//ASSIGNMENT-2: Using CompletableFuture
package com.brevitaz.multithreading;

import java.util.Scanner;
import java.util.concurrent.CompletableFuture;

public class Exercise1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please provide first value [Integer ] : ");
        int num1 = scanner.nextInt();
        System.out.println("Please provide second value [Integer ] : ");
        int num2 = scanner.nextInt();
        Exercise1 ex1 = new Exercise1();
        CompletableFuture.runAsync(()-> System.out.println( "Sum of a and b is "+add(num1,num2)))
                .thenRunAsync(()-> System.out.println( "Difference of a and b is "+subtract(num1,num2)))
                .thenRunAsync(()-> System.out.println( "Product of a and b is "+ ex1.multiply(num1,num2)))
                .thenRunAsync(()-> System.out.println( "Division of a and b is "+ex1.divide(num1,num2)))
                .thenApplyAsync(sumofall-> add(num1,num2)+subtract(num1,num2)+ ex1.multiply(num1,num2)+ ex1.divide(num1,num2))
                .thenAcceptAsync(sumofall-> System.out.println("Total of all operations is "+ sumofall)).join();
    }
    public static int add(int a, int b) {
        return a+b;
    }
    public static int subtract(int a, int b) {
        return a-b;
    }
    public int multiply(int a,int b) {
        return a*b;
    }
    public int divide(int a,int b) {
        return a/b;
    }
}
