/*
* ASSIGNMENT-2: Write the program that takes any two numbers as input from the user,
* perform mathematical operations on that numbers and provide the output in the below-given format:
* Note: all mathematical operations must be done in individual threads.
* Ex:
* Inputs:
*
* Input-1:  Please provide first value [Integer ] : 4

* Input-2:  Please provide first value [Integer ] : 2

* Output:

* Addition of 4 and 2 is: 6
* Subtraction of 4 and 2 is : 2
* Multiplication of 4 and 2 is : 8
* Division of 4 and 2 is : 2
* Total sum of all operation result value is : 18
*/
package com.brevitaz.multithreading;

import java.util.Scanner;

public class Assignment2 {
    private int sumOfAll;
    static int num1;
    static int num2;
    private int sum = 0;
    private int difference;
    private int product;
    private int quotient;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please provide first value [Integer ] : ");
        num1 = scanner.nextInt();
        System.out.println("Please provide second value [Integer ] : ");
        num2 = scanner.nextInt();
        Assignment2 assignment2 = new Assignment2();
        assignment2.doArithmeticOperations();
    }
    public synchronized int addAll() {
        return sum+difference+product+quotient;
    }
    public void doArithmeticOperations(){
        Thread t1 = new Thread() {
            public void run(){
                sum = num1+num2;
                System.out.println("Addition of "+ num1 +" and " + num2 +" is "+ sum );
                Thread t2 = new Thread() {
                    public void run(){
                        difference = num1-num2;
                        System.out.println("Subtraction of "+ num1 +" and " + num2 +" is "+ difference );
                        Thread t3 = new Thread() {
                            public void run(){
                                product = num1*num2;
                                System.out.println("Multiplication of "+ num1 +" and " + num2 +" is "+ product );
                                Thread t4 = new Thread() {
                                    public void run(){
                                        quotient = num1/num2;
                                        System.out.println("Division of "+ num1 +" and " + num2 +" is "+ quotient );
                                        Thread t5 = new Thread(){
                                          public void run(){
                                              sumOfAll = sum+difference+product+quotient;
                                              System.out.println("Total sum of all the operation's result value is :" +sumOfAll);
                                          }
                                        };
                                        t5.start();
                                    }
                                };
                                t4.start();
                            }
                        };
                        t3.start();
                    }
                };
                t2.start();
            }
        };

        t1.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
